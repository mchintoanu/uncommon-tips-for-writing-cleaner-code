package net.example.codecamp;


/**
 *
 */
public class Point {

    private final Latitude latitude;
    private final Longitude longitude;

    public Point(final Latitude latitude, final Longitude longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Latitude latitude() {
        return latitude;
    }

    public Longitude longitude() {
        return longitude;
    }

    public Length distanceTo(final Point other) {
        return new DistanceComputer().apply(this, other);
    }
}
