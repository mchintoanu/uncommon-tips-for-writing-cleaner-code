package net.example.codecamp;


/**
 *
 */
public class Latitude {

    public static Latitude degrees(double degrees) {
        return new Latitude(degrees);
    }

    public static Latitude radians(double radians) {
        return new Latitude(Math.toDegrees(radians));
    }


    private final double degrees;

    private Latitude(final double degrees) {
        if (degrees < -90 || degrees > 90) {
            throw new IllegalArgumentException("latitude value out of range");
        }
        this.degrees = degrees;
    }

    public double asDegrees() {
        return degrees;
    }

    public double asRadians() {
        return Math.toRadians(degrees);
    }
}
