package net.example.codecamp;


import java.util.function.BiFunction;


/**
 *
 */
class DistanceComputer implements BiFunction<Point, Point, Length> {

    private static final Length EARTH_RADIUS = Length.kilometers(6372.8);


    @Override
    public Length apply(final Point point1, final Point point2) {
        double latitudeDifference = point2.latitude().asRadians() - point1.latitude().asRadians();
        double longitudeDifference = point2.longitude().asRadians() - point1.longitude().asRadians();
        double latitude1 = point1.latitude().asRadians();
        double latitude2 = point2.latitude().asRadians();

        double a = Math.pow(Math.sin(latitudeDifference / 2), 2)
                + Math.pow(Math.sin(longitudeDifference / 2), 2) * Math.cos(latitude1) * Math.cos(latitude2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return EARTH_RADIUS.multipliedBy(c);
    }
}
