# Uncommon Tips for Writing Cleaner Code

This project contains the code from the live coding sessions of my presentation at Codecamp, The One with Java, from 14 April 2022.

Presentation title: "Uncommon Tips for Writing Cleaner Code"

## License

Copyright (c) 2022, Mihai Chintoanu. All rights reserved.

This code is free software. You may use it in compliance with the GNU Lesser General Public License (LGPL), Version 3, or any later version. You may obtain a copy of the License at http://www.gnu.org/licenses/lgpl.html

This code is distributed on an "AS IS" basis, WITHOUT ANY WARRANTY.
