package net.example.codecamp;


/**
 *
 */
public class Length {

    public static Length meters(final double meters) {
        return new Length(meters);
    }

    public static Length kilometers(final double kilometers) {
        return new Length(kilometers * 1000);
    }


    private final double meters;

    private Length(final double meters) {
        this.meters = meters;
    }

    public double asMeters() {
        return meters;
    }

    public double asKilometers() {
        return meters / 1000;
    }

    public Length plus(final Length other) {
        return new Length(meters + other.meters);
    }

    public Length multipliedBy(final double constant) {
        return new Length(meters * constant);
    }
}
