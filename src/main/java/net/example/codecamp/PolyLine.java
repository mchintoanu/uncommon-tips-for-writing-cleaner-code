package net.example.codecamp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


/**
 *
 */
public class PolyLine {

    private final List<Point> points;

    public PolyLine(final Point... points) {
        this(Arrays.asList(points));
    }

    public PolyLine(final List<Point> points) {
        if (points == null) {
            throw new IllegalArgumentException("point list is null");
        }
        if (points.isEmpty()) {
            throw new IllegalArgumentException("point list is empty");
        }
        this.points = new ArrayList<>(points);
    }

    public Length length() {
        final Iterator<Point> iterator = points.iterator();
        Point current = iterator.next();
        Length length = Length.meters(0);
        while (iterator.hasNext()) {
            final Point next = iterator.next();
            final Length segmentLength = new DistanceComputer().apply(current, next);
            length = length.plus(segmentLength);
            current = next;
        }
        return length;
    }
}
