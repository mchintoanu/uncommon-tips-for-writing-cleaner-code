package net.example.codecamp;


/**
 *
 */
public class Main {

    public static void main(String[] args) {
        final Point point1 = new Point(Latitude.degrees(46.7), Longitude.degrees(23.6));
        final Point point2 = new Point(Latitude.degrees(60.1), Longitude.degrees(24.9));
        final Length distance = point1.distanceTo(point2);
        System.out.println("Distance: " + distance.asKilometers() + " km");

        final PolyLine line = new PolyLine(point1, point2, point1);
        System.out.println("Line length: " + line.length().asKilometers() + " km");
    }
}
