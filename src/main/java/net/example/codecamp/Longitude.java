package net.example.codecamp;


/**
 *
 */
public class Longitude {

    public static Longitude degrees(double degrees) {
        return new Longitude(degrees);
    }

    public static Longitude radians(double radians) {
        return new Longitude(Math.toDegrees(radians));
    }
    

    private final double degrees;

    private Longitude(final double degrees) {
        if (degrees < -180 || degrees > 180) {
            throw new IllegalArgumentException("longitude value out of range");
        }
        this.degrees = degrees;
    }

    public double asDegrees() {
        return degrees;
    }

    public double asRadians() {
        return Math.toRadians(degrees);
    }
}
